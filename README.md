**Opensilecs Eclipse Plugin** is an Eclipse plugin which simplifies usage of [Opensilecs](https://git.gsi.de/silecs/opensilecs) to configure and access PLC systems via FESA.

## build with maven

If you want to install Silecs Eclipse Plugin locally from sources (e.g. for development purposes), follow these steps:

1. Clone repository.
```
git clone https://git.gsi.de/silecs/opensilecs-eclipse-plugin.git
```

2. Build plugin by running `build.sh` script in main directory.
```
opensilecs-eclipse-plugin$ sh build.sh
```

## run via eclipse

- **Make sure the correct Eclipse platform is defined as target platform:**

```
Window --> Preferences --> Plug In Development --> Tatrget Platform
Add --> Default (...) --> Finsih --> Apply & Close
```

<img alt="Target Platform" src="static/target-platform.PNG" width=60%>

- **Run plugin from source folder**
```
Run --> Run Configurations --> Eclipse Application --> silecs.eclipse.plugin.product --> Run
```
## Local Installation

- **Define a new version**

Define version of feature in file `feature.xml` in `silecs-eclipse-plugin-gsi-feature`.

<img alt="Define feature version " src="static/feature.PNG" width=60%>

- **Trigger the build of the new version**

Open `site.xml`, add new category, select feature created in previous step.
Build new feature using `Build` button

<img alt="Update site" src="static/site.PNG" width=60%>

- **Fix possible build errors**

If there are build errors, try to follow the solutions suggested by eclipse. (E.g. here the target Java Version was not available any more)

<img alt="Error in manifest" src="static/MANIFEST1.PNG" width=60%>

<img alt="Suggested Fix" src="static/MANIFEST2.PNG" width=60%>

- **Install the plugin**

Reopen eclipse (Otherwise the new plugin version will not be found)! Go to `Help` -> `Install New Software...`.
Add new site by clicking `Add`. In pop up window, click on `Local` and choose location of directory `silecs-eclipse-plugin-gsi-update-site`. Press `Add`.
Choose previously created feature. Install it by pressing `Next` accepting License and `Finish`. If there will be Security warning choose `Install anyway`. To apply changes, restart of eclipse is required.

<img alt="Install" src="static/install.PNG" width=60%>


## Global Installation

Once you followed the steps for the local Installation and you made sure that it installs fine, the following script will copy the plugin to the global default install folder:
```
./silecs-eclipse-plugin-gsi-update-site/install.sh
```
Optionally you can pass a different install path as parameter
