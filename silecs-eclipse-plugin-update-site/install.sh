#!/bin/sh
set -e

GLOBAL_RELEASE_DIR=/common/usr/cscofe/silecs/silecs-eclipse-plugin-gsi-update-site

if [ -z "$1" ]
  then
    RELEASE_DIR=$GLOBAL_RELEASE_DIR
  else
    RELEASE_DIR=$1
fi

SCRIPT=$(readlink -f "$0")
SCRIPTPATH=$(dirname "$SCRIPT")     # path where this script is located in

mkdir -p ${RELEASE_DIR}
cp -ruv ${SCRIPTPATH}/* ${RELEASE_DIR}
