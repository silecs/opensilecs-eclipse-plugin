package silecs.utils;

//Class to keep result of operation with message
public class ResultPair {

    private boolean result;
    private String message;

    public ResultPair(boolean result, String message) {
        this.result = result;
        this.message = message;
    }

    public boolean getResult() {
        return this.result;
    }

    public String getMessage() {
        return this.message;
    }
}