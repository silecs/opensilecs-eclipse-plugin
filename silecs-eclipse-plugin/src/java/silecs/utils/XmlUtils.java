// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.utils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.xml.core.internal.contentmodel.modelquery.ModelQuery;
import org.eclipse.wst.xml.core.internal.contentmodel.modelqueryimpl.CMDocumentLoader;
import org.eclipse.wst.xml.core.internal.contentmodel.modelqueryimpl.InferredGrammarBuildingCMDocumentLoader;
import org.eclipse.wst.xml.core.internal.modelquery.ModelQueryUtil;

import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

@SuppressWarnings("restriction")
public class XmlUtils {

    private static TransformerFactory TRANSFORMER_FACTORY;

    public static void replaceFesaVersionInFesaDocument(String fesaDocumentpath, String newFesaVersion)
            throws ParserConfigurationException, SAXException, IOException, TransformerFactoryConfigurationError,
            TransformerException {
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fesaDocumentpath);
        NodeList nodes = doc.getElementsByTagName("fesa-version");
        for (int i = 0; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Node newNode = doc.createElement("fesa-version");
            newNode.setTextContent(newFesaVersion);
            node.getParentNode().replaceChild(newNode, node);
        }

        // write changes back to file
        Transformer tr = createPrettyTransformer();
        tr.transform(new DOMSource(doc), new StreamResult(new FileOutputStream(fesaDocumentpath)));
    }

    public static String nodeToString(Node node) throws TransformerFactoryConfigurationError, TransformerException {
        Transformer transformer = createPrettyTransformer();

        // if given node is not a complete document, we do not
        // put the xml declaration. we also omit if the document already has
        // a type
        if (!(node instanceof Document) || ((Document) node).getDoctype() != null) {
            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        }

        // write node to string
        DOMSource source = new DOMSource(node);
        StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);

        return writer.toString();
    }

    private static Transformer createPrettyTransformer()
            throws TransformerConfigurationException, TransformerFactoryConfigurationError {
        if (TRANSFORMER_FACTORY == null) {
            TRANSFORMER_FACTORY = TransformerFactory.newInstance();
        }
        Transformer t = TRANSFORMER_FACTORY.newTransformer();
        t.setOutputProperty(OutputKeys.METHOD, "xml");
        t.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

        return t;
    }

    public static String xPathFromNode(Node node) {
        if (node == null) {
            return "";
        }

        switch (node.getNodeType()) {
        case Node.DOCUMENT_NODE:
            return "";

        case Node.ATTRIBUTE_NODE:
            Attr attr = (Attr) node;
            return xPathFromNode(attr.getOwnerElement()) + "/@" + attr.getName();

        case Node.ELEMENT_NODE:
            Element elem = (Element) node;
            Node parent = elem.getParentNode();
            String name = elem.getTagName();
            NodeList childs = parent.getChildNodes();

            int index = 1;
            for (int i = 0; i < childs.getLength(); i++) {
                Node n = childs.item(i);
                if (n.equals(elem)) {
                    // first element in xPath is 1, not 0
                    return xPathFromNode(parent) + "/" + node.getNodeName() + "[" + index + "]";
                } else if (n instanceof Element && n.getNodeName().equals(name)) {
                    // there is another element with the same name before in the list
                    index++;
                }
            }

        default:
            throw new IllegalStateException("Unexpected Node type: " + node.getNodeType());
        }
    }

    /**
     * Return the string contained in the string builder. Remove the ending if
     * equals to the parameter (use for joining strings in loops)
     * 
     * @param stringBuilder
     * @param endingToRemove
     * @return
     */
    public static String finalize(StringBuilder stringBuilder, String endingToRemove) {
        if (stringBuilder == null) {
            throw new IllegalArgumentException("stringBuilder cannot be null");
        } else if (endingToRemove == null) {
            throw new IllegalArgumentException("endingToRemove cannot be null");
        }

        int length = stringBuilder.length();
        int endingLength = endingToRemove.length();
        if (length > endingLength && stringBuilder.substring(length - endingLength).equals(endingToRemove)) {
            return stringBuilder.substring(0, length - endingLength);
        } else {
            return stringBuilder.toString();
        }
    }

    public static void reloadDependencies() {
        List<IEditorReference> editors = new ArrayList<>();
        for (IWorkbenchWindow window : PlatformUI.getWorkbench().getWorkbenchWindows()) {
            for (IWorkbenchPage page : window.getPages()) {
                editors.addAll(Arrays.asList(page.getEditorReferences()));
            }
        }

        for (IEditorReference er : editors) {
            IEditorPart editor = er.getEditor(false);
            ITextEditor textEditor = null;
            if (editor instanceof ITextEditor)
                textEditor = (ITextEditor) editor;
            else if (editor != null) {
                Object o = editor.getAdapter(ITextEditor.class);
                if (o != null)
                    textEditor = (ITextEditor) o;
            }
            if (textEditor != null) {
                IDocument document = textEditor.getDocumentProvider().getDocument(textEditor.getEditorInput());
                IStructuredModel model = StructuredModelManager.getModelManager().getExistingModelForRead(document);
                if (model != null) {
                    ModelQuery modelQuery = null;
                    try {
                        modelQuery = ModelQueryUtil.getModelQuery(model);
                    } finally {
                        model.releaseFromRead();
                    }
                    Document domDocument = ((IDOMModel) model).getDocument();
                    if ((modelQuery != null) && (modelQuery.getCMDocumentManager() != null)) {
                        modelQuery.getCMDocumentManager().getCMDocumentCache().clear();
                        CMDocumentLoader loader = new InferredGrammarBuildingCMDocumentLoader(domDocument, modelQuery);
                        loader.loadCMDocuments();
                    }
                }
            }
        }
    }
}
