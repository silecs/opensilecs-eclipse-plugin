package silecs.utils;

import silecs.model.exception.SilecsException;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * FesaUtils class
 * This is set of utilities that simplify generating FESA classes from
 * .silecsdesign and .silecsdeploy files
 */
public class FesaUtils {
    public static final String DEFAULT_FESA_BASE_PATH = "/opt/fesa";

    public static String getDefaultFesaBasePath()
    {
        return DEFAULT_FESA_BASE_PATH;
    }

    /**
     * Gets list of FESA template files.
     * @param type "design" or "deployment". Type of the file that template refers to.
     * @return Array of paths to the templates
     * @throws SilecsException when no templates are found
     */
    protected static File[] getFESATemplates(String type) throws SilecsException
    {
        String fesaBaseDirectory = MainPreferencePage.getFESADirectory();
        File folderModel = new File(fesaBaseDirectory + "/fesa-fwk/" + MainPreferencePage.getFESAVersion() + "/fesa-model/xml/" + type + "/templates");
        File folderModelGSI = new File(fesaBaseDirectory + "/fesa-fwk/" + MainPreferencePage.getFESAVersion() + "/fesa-model-gsi/xml/" + type + "/templates");

        File[] templatesModel = folderModel.listFiles();
        if (templatesModel == null)
            templatesModel = new File[0];

        File[] templatesModelGSI = folderModelGSI.listFiles();
        if (templatesModelGSI == null)
            templatesModelGSI = new File[0];

        if ( (templatesModel.length + templatesModelGSI.length) == 0 )
        {
            ConsoleHandler.printMessage("Template search folder 1: " + folderModel.getPath());
            ConsoleHandler.printMessage("Template search folder 2: " + folderModelGSI.getPath());
            throw new SilecsException("No templates found there! Please fix the 'FESA Base Path' in the Preferences!");
        }
        File[] templates = new File[templatesModel.length + templatesModelGSI.length];
        System.arraycopy(templatesModel, 0, templates, 0, templatesModel.length);
        System.arraycopy(templatesModelGSI, 0, templates, templatesModel.length, templatesModelGSI.length);
        return templates;
    }

    public static File[] getFesaDesignTemplates() throws SilecsException
    {
        return getFESATemplates("design");
    }

    public static File[] getFesaDeployUnitTemplates() throws SilecsException
    {
        return getFESATemplates("deployment");
    }

    public static List<Version> readOutAvailableFesaVersions() throws SilecsException
    {
        String fesaBaseFolderString = MainPreferencePage.getFESADirectory() + "/fesa-fwk";
        File fesaBaseFolder = new File(fesaBaseFolderString);

        List<Version> fesaVersions = new ArrayList<Version>();

        if(!fesaBaseFolder.exists())
            return fesaVersions;

        for (final File fileEntry : fesaBaseFolder.listFiles()) {
            try {
                Version version = new Version(fileEntry.getName());
                fesaVersions.add(version);
            } catch (Exception ex) {
                ConsoleHandler.printError("Failed to read FESA-Version: " + fileEntry.getName());
                ConsoleHandler.printStackTrace(ex);
            }
        }
        return fesaVersions;
    }
}
