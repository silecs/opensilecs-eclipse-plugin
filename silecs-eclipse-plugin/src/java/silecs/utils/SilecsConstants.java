// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.utils;

import java.util.Arrays;
import java.util.List;

public class SilecsConstants {

    public enum ProjectType {
        DESIGN_PROJECT, DEPLOY_PROJECT
    }

    public static final String SILECS_NAME = "Silecs-Eclipse-Plugin";

    // consistency check
    public static final String DESIGN_CLASS_NODE = "SILECS-Class";

    public static final String DEPLOY_UNIT_NODE = "Deploy-Unit";

    public static final String NAME = "name";

    public static final String HOST_NAME = "host-name";

    // schema files
    public static final String DESIGN_SCHEMA_XSD = "DesignSchema.xsd";

    public static final String DEPLOY_SCHEMA_XSD = "DeploySchema.xsd";

    // file extensions
    public static final String IEDESIGN_FILE = "silecsdesign";

    public static final String IEDEPLOY_FILE = "silecsdeploy";

    // name patterns
    public static final String DESIGN_PATTERN = "[A-Z][A-Za-z0-9_]*";

    public static final String DEPLOY_PATTERN = "[A-Z][A-Za-z0-9_]*";

    public static final int MAX_DESIGN_LENGTH = 20;

    public static final int MAX_DEPLOY_LENGTH = 20;

    // SILECS versions which are supported by this plugin
    public static final String[] SUPPORTED_SILECS_MAJOR_VERSIONS = { "2", "1", "DEV" };
    public static final List<String> ADDITIONAL_SILECS_TARGET_MIGRATION_VERSIONS = Arrays.asList("0.10.0"); // e.g. in
                                                                                                            // order to
                                                                                                            // allow
                                                                                                            // migration
                                                                                                            // from
                                                                                                            // 0.9.0 to
                                                                                                            // 0.10.0
    public static final int DEFAULT_SILECS_VERSION_INDEX = 0;

    // SILECS Versions which are NOT supported by this plugin
    // All other versions foudn in the silecs-search path are supported
    public static final String[] SILECS_VERSION_BLACKLIST = { "0.9.0" };

    // Wizards
    // New Design Wizard
    public static final String NEW_DESIGN_WIZARD_TITLE = "New Design Wizard";
    public static final String NEW_DESIGN_PAGE_TITLE = "New Design Project";

    // New Deploy Wizard
    public static final String NEW_DEPLOY_WIZARD_TITLE = "New Deploy Wizard";
    public static final String NEW_DEPLOY_PAGE_TITLE = "New Deploy Project";

    // New Class Wizard Page

    public static final String INVAILD_PROJECT_NAME = "Invaild project name. Project name must follow pattern:";

    public static final String INITIAL_PROJECT_VERSION = "1.0.0";

    // Generate Fesa Class Wizard
    public static final String GENERATE_FESA_CLASS_WIZARD_TITLE = "Generate Fesa Class Wizard";
    public static final String GENERATE_FESA_CLASS_PAGE_TITLE = "Generate Fesa Class";

    // Generate Client Resources Wizard
    public static final String GENERATE_CLIENT_RESOURCES_WIZARD_TITLE = "Generate Client Resources Wizard";
    public static final String GENERATE_CLIENT_RESOURCES_PAGE_TITLE = "Generate Client Resources";

    // Deliver Client Resources Wizard
    public static final String DELIVER_CLIENT_RESOURCES_WIZARD_TITLE = "Deliver Client Resources Wizard";
    public static final String DELIVER_CLIENT_RESOURCES_PAGE_TITLE = "Deliver Client Resources";
}
