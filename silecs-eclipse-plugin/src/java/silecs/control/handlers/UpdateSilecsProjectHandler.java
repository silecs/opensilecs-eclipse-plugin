// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import silecs.model.exception.SilecsException;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;
import silecs.view.console.ConsoleHandler;
import silecs.view.job.BaseProjectJob;

import silecs.view.job.UpdateVersionJob;
import silecs.view.wizards.UpdateSilecsProjectWizard;

public class UpdateSilecsProjectHandler extends AbstractHandler implements IHandler {

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {

        ConsoleHandler.clear();

        BaseProjectJob job = null;

        try {
            Version newVersion = pickVersion(event);
            IFile silecsFile = SilecsUtils.extractSilecsFileFromEvent(event);
            IProject project = silecsFile.getProject();
            job = new UpdateVersionJob(project, newVersion);

        } catch (Exception e) {
            ConsoleHandler.printError("Failed to update SILECS version: ");
            ConsoleHandler.printStackTrace(e);
            return null;
        }

        job.setUser(true);
        job.schedule();

        return null;
    }

    private Version pickVersion(ExecutionEvent event) throws SilecsException {
        Shell activeShell = HandlerUtil.getActiveShell(event);
        IWizard wizard = new UpdateSilecsProjectWizard();
        WizardDialog dialog = new WizardDialog(activeShell, wizard);
        dialog.open();
        return new Version(((UpdateSilecsProjectWizard) wizard).getSelectedVersion());
    }
}
