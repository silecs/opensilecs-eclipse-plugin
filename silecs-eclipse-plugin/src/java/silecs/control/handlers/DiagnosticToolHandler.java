// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.handlers;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;

import silecs.control.core.DeployProjectNature;
import silecs.utils.OSExecute;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;

public class DiagnosticToolHandler extends AbstractHandler implements IHandler {

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {

        try {
            ConsoleHandler.clear();

            IProject project = SilecsUtils.extractSilecsFileFromEvent(event).getProject();

            if (!SilecsUtils.hasSilecsDeploy(project))
                throw new Exception("No '.silecsdeploy' found in project" + project.getName());

            SilecsUtils.checkSilecsVersion(project);
            IFile deployFile = SilecsUtils.getSilecsDeployFile(project);
            Version silecsVersion = SilecsUtils.getSilecsVersionFromFile(deployFile);

            String binary = MainPreferencePage.getDiagToolBasePath(silecsVersion) + "/bin/x86_64/silecs-diagnostic";
            String snap7lib = MainPreferencePage.getSNAP7LibraryBasePath(silecsVersion) + "/bin/x86_64-linux";

            ConsoleHandler.printMessage("Starting  Silecs Diagnostic Tool from:" + binary);

            String[] command = new String[3];
            command[0] = binary;
            command[1] = "-d";
            command[2] = deployFile.getRawLocation().makeAbsolute().toOSString();
            String envVarName = "LD_LIBRARY_PATH";
            Map<String, String> env = new HashMap<String, String>();
            env.put(envVarName, snap7lib);
            ConsoleHandler.printMessage("Setting environment variable: '" + envVarName + "' to '" + snap7lib + "'");
            ConsoleHandler.printMessage("Executing command: " + command[0] + " " + command[1] + " " + command[2]);
            OSExecute.executeCommandDetached(env, command);
        } catch (Exception e) {
            ConsoleHandler.printError("Could not launch Silecs Diagnostic Tool");
            e.printStackTrace();
        }

        return null;
    }

}
