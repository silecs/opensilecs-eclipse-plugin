// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;

import silecs.utils.SilecsUtils;
import silecs.view.console.ConsoleHandler;

import java.awt.Desktop;
import java.net.URI;

/**
 * This handler opens silecs-wiki in the browser.
 */
public class SilecsWikis extends AbstractHandler implements IHandler {

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {

        // Open windows default browser (or firefox under linux) and load Silecs Wikis
        // pages
        try {
            if (SilecsUtils.isWindows)
            {
                Desktop.getDesktop().browse(new URI(SilecsUtils.getSilecsWiki()));
            }
            else
            {
                Runtime runtime = Runtime.getRuntime();
                runtime.exec("/usr/bin/firefox -new-window " + SilecsUtils.getSilecsWiki());
            }
        } catch (Exception e) {
            e.printStackTrace();
            ConsoleHandler.printError("Failed to open Silecs-Wiki in browser. Reason:" + e.getMessage());
            ConsoleHandler.printStackTrace(e);
        }
        return null;
    }
}
