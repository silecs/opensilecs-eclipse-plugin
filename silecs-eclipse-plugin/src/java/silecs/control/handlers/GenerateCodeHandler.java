// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.handlers;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.commands.IHandler;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.jface.wizard.IWizard;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.ui.handlers.HandlerUtil;

import silecs.control.core.DeployProjectNature;
import silecs.control.core.DesignProjectNature;
import silecs.model.exception.SilecsException;
import silecs.utils.FesaUtils;
import silecs.utils.SilecsUtils;
import silecs.utils.XmlUtils;
import silecs.view.console.ConsoleHandler;
import silecs.view.job.BaseProjectJob;
import silecs.view.job.GenerateClassJob;
import silecs.view.job.GenerateDeployJob;
import silecs.view.preferences.MainPreferencePage;
import silecs.view.wizards.GenerateCodeWizard;

public class GenerateCodeHandler extends BaseHandler implements IHandler {

    @Override
    public Object execute(ExecutionEvent event) throws ExecutionException {

        ConsoleHandler.clear();

        BaseProjectJob job = null;
        try {
            IFile silecsFile = SilecsUtils.extractSilecsFileFromEvent(event);
            IProject project = silecsFile.getProject();
            SilecsUtils.checkSilecsVersion(project);
            
        	IFolder src = project.getFolder("src");
			if(SilecsUtils.hasSilecsDesign(project) )
			{
	    		IFile fesaDesignDoc = src.getFile(project.getName() + ".design");
	    		boolean designDocExists = fesaDesignDoc.exists();
	    		if(!designDocExists)
	    		{
	        		File template = pickTemplate(event, FesaUtils.getFesaDesignTemplates());
	        		if( template == null) // cancel pressed
	        			return null;
	        		ConsoleHandler.printMessage("The following template will be used:" + template.getAbsolutePath());
	    		    InputStream source = new FileInputStream(template);
	    		    fesaDesignDoc.create(source, IResource.NONE, null);
	    		}
				job = new GenerateClassJob(project, silecsFile, fesaDesignDoc);
			}
			else if(SilecsUtils.hasSilecsDeploy(project) )
			{
	    		IFile fesaDeployDoc = src.getFile(project.getName() + ".deploy");
	    		boolean deployDocExists = fesaDeployDoc.exists();
	    		if(!deployDocExists)
	    		{
	        		File template = pickTemplate(event,FesaUtils.getFesaDeployUnitTemplates());
	        		if( template == null) // cancel pressed
	        			return null;
	        		ConsoleHandler.printMessage("The following template will be used:" + template.getAbsolutePath());
	    		    InputStream source = new FileInputStream(template);
	    		    fesaDeployDoc.create(source, IResource.NONE, null);
	    		    
	    		    // For deploy units, it is required to replace the FESA Version
	    		    XmlUtils.replaceFesaVersionInFesaDocument(fesaDeployDoc.getRawLocation().toString(), MainPreferencePage.getFESAVersion());
	    		}
	    		
				job = new GenerateDeployJob(project, fesaDeployDoc,!deployDocExists);
			}
			else
			{
				ConsoleHandler.printError("Failed to find SILECS Design/Deploy File");
				return null;
			}
		} catch (Exception e)
        {
			ConsoleHandler.printError("Failed to generate code: ");
			ConsoleHandler.printStackTrace(e);
	        return null;
		}
            	
        job.setUser(true);
        job.schedule();

        return null;
    }

    private File pickTemplate(ExecutionEvent event, File[] templates) throws SilecsException {
        if (templates.length < 1) {
            String error = "Failed to generate FESA Class. No templates found. please fix your FESA-PATH in the preferences!";
            ConsoleHandler.printError(error);
            throw new SilecsException(error);
        }
        if (templates.length == 1) {
            return templates[0];
        }
        // TODO: Would be nice to add the possibility to pick a foreign template from
        // the file system
        Shell activeShell = HandlerUtil.getActiveShell(event);
        IWizard wizard = new GenerateCodeWizard(templates);
        WizardDialog dialog = new WizardDialog(activeShell, wizard);
        dialog.open();
        return ((GenerateCodeWizard) wizard).getChoice();
    }

}
