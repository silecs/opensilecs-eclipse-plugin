// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.handlers;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.ui.ide.IDE;

public abstract class BaseHandler extends AbstractHandler {

    /**
     * Gives the list of selected project
     * 
     * @param event
     * @return
     */
    protected List<IProject> retrieveProjects(ExecutionEvent event) {
        List<IProject> projects = new ArrayList<IProject>();

        // get from where the command was fired
        Object trigger = event.getTrigger();
        if (trigger instanceof Event) {
            Event ev = (Event) trigger;

            if (ev.widget instanceof MenuItem) {
                IStructuredSelection selection = (IStructuredSelection) HandlerUtil.getActiveMenuSelection(event);
                List<?> resources = IDE.computeSelectedResources(selection);
                for (Object o : resources) {
                    if (o instanceof IProject) {
                        projects.add((IProject) o);
                    } else if (o instanceof IFile) {
                        IProject project = ((IFile) o).getProject();

                        if (project != null) {
                            projects.add(project);
                        }
                    }
                }
            } else {
                IEditorPart editor = HandlerUtil.getActiveEditor(event);
                if (editor == null)
                    return projects;
                IFileEditorInput input = (IFileEditorInput) editor.getEditorInput();
                projects.add(input.getFile().getProject());
            }
        }

        if (projects.isEmpty()) {
            // LOGGER.info("No projects could be retrieved");
        }

        return projects;
    }
}
