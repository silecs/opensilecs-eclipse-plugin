// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.core;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;

import silecs.view.console.ConsoleHandler;

public class DesignProjectNature extends SilecsProjectNature implements IProjectNature {

    public static final String NATURE_ID = "silecs.control.core.designprojectnature";

    private static final String CERN_FESA_PLUGIN_CORE_FESA_CLASS_PROJECT_NATURE = "fesa.plugin.core.fesaClassProjectNature";

    public static void addClassNature(IProject project, IProgressMonitor mon) throws CoreException {
        IProgressMonitor monitor = mon;
        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }

        try {
            monitor.beginTask("Add silecs class nature", 2);
            addNature(project, NATURE_ID, SubMonitor.convert(monitor, 1));
            addCppNature(project, SubMonitor.convert(monitor, 1));
        } finally {
            monitor.done();
        }
    }

    public static void removeClassNature(IProject project, IProgressMonitor mon) throws CoreException {
        IProgressMonitor monitor = mon;
        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }

        try {
            monitor.beginTask("Remove silecs class nature", 2);
            removeNature(project, NATURE_ID, SubMonitor.convert(monitor, 1));
            removeCppNature(project, SubMonitor.convert(monitor, 1));
        } finally {
            monitor.done();
        }
    }

    public static void convertToClassNature(IProject project, IProgressMonitor mon) throws CoreException {
        IProgressMonitor monitor = mon;
        if (monitor == null) {
            monitor = new NullProgressMonitor();
        }

        try {
            monitor.beginTask("Converting project to silecs class nature", 2);
            removeAllNatures(project, SubMonitor.convert(monitor, 1));
            addClassNature(project, SubMonitor.convert(monitor, 1));
        } finally {
            monitor.done();
        }
    }

    public static void addFESAClassNature(IProject project, IProgressMonitor mon) throws CoreException {
        IProgressMonitor monitor = mon;
        if (mon == null) {
            monitor = new NullProgressMonitor();
        }
        monitor.beginTask("Adding FESA Class Nature to project", 2);
        try {
            addNature(project, CERN_FESA_PLUGIN_CORE_FESA_CLASS_PROJECT_NATURE, SubMonitor.convert(monitor, 1));
            addManageBuilderNature(project, mon);
        } finally {
            monitor.done();
        }
    }

    public static void removeFESAClassNature(IProject project, IProgressMonitor mon) throws CoreException {
        IProgressMonitor monitor = mon;
        if (mon == null) {
            monitor = new NullProgressMonitor();
        }
        monitor.beginTask("Removing FESA Nature from project", 2);
        try {
            removeNature(project, CERN_FESA_PLUGIN_CORE_FESA_CLASS_PROJECT_NATURE, SubMonitor.convert(monitor, 1));
            removeManageBuilderNature(project, mon);
        } finally {
            monitor.done();
        }
    }

}
