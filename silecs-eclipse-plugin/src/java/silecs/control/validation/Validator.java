// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.control.validation;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.xerces.parsers.DOMParser;
import org.apache.xerces.xni.parser.XMLInputSource;
import org.eclipse.core.resources.IFile;

import silecs.control.validation.internal.Handler;
import silecs.model.document.SilecsDocumentError;
import silecs.utils.OSExecute;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;

/**
 * Class used for validating single xmlFile
 */
public class Validator {
    public static final String INVALID_FILE_MESSAGE = "File is not valid";

    /**
     * This method takes as an argument xml file.<br>
     * If the document is not xml file or is somehow broken list with "Document is
     * not parsed correctly" error is returned."<br>
     * 
     * External python validation is called in this method!
     * 
     * @param xmlFile
     * @return list of errors
     */
    public static List<SilecsDocumentError> validate(IFile xmlFile) {
        List<SilecsDocumentError> errors = new ArrayList<>();
        try {
            // create DOM parser which validates, use a grammar pool,
            // support xincludes and allow the access to the nodes
            // during the parsing
            DOMParser parser = new DOMParser();

            parser.setFeature("http://xml.org/sax/features/validation", true);
            parser.setFeature("http://apache.org/xml/features/validation/schema", true);
            parser.setFeature("http://apache.org/xml/features/honour-all-schemaLocations", true);
            parser.setFeature("http://apache.org/xml/features/xinclude", true);
            parser.setFeature("http://apache.org/xml/features/xinclude/fixup-base-uris", false);
            parser.setFeature("http://apache.org/xml/features/xinclude/fixup-language", false);
            parser.setFeature("http://apache.org/xml/features/dom/defer-node-expansion", false);

            // Set the error handler
            File file = new File(xmlFile.getLocationURI());
            Handler handler = new Handler(xmlFile, parser);
            parser.setErrorHandler(handler);

            Version silecsVersion = SilecsUtils.getSilecsVersionFromFile(xmlFile);
            String silecsCliScript = MainPreferencePage.getSilecsCliPath(silecsVersion);
            String[] command;
            String fileAbsPath = xmlFile.getRawLocation().toOSString();
            command = new String[] { silecsCliScript, "-v", fileAbsPath };

            Process process = Runtime.getRuntime().exec(command);
            process.waitFor();

            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                ConsoleHandler.printMessage(line);
                if (line.trim().contains(INVALID_FILE_MESSAGE))
                    errors.add(new SilecsDocumentError(SilecsDocumentError.ErrorLevel.ERROR, line, -1, -1, null));
            }

            input.close();

            BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            while ((line = error.readLine()) != null) {
                ConsoleHandler.printError(line);
                if (line.trim().contains(INVALID_FILE_MESSAGE))
                    ;
                errors.add(new SilecsDocumentError(SilecsDocumentError.ErrorLevel.ERROR, line, -1, -1, null));
            }

            error.close();
        } catch (Exception e) {
            ConsoleHandler
                    .printError("Document " + xmlFile.getName() + " has not been parsed correctly: " + e.getMessage());
            ConsoleHandler.printStackTrace(e);
        }

        return errors;
    }

    public static boolean isDocumentValid(IFile silecsDocument) {
        try {
            if (validate(silecsDocument).isEmpty())
                return true;
            return false;
        } catch (Exception e) {
            e.printStackTrace();
            ConsoleHandler.printStackTrace(e);
            return false;
        }
    }
}
