// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.model.document;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import silecs.model.exception.SilecsException;
import silecs.utils.XmlUtils;
import silecs.view.console.ConsoleHandler;

public class XmlBasedDocument {

    public static final String XML_EXTENSION = ".xml";
    public static final String XSD_EXTENSION = ".xsd";

    protected Document xmlDocument;

    /**
     * Abstract class for data access object based on xml DOM
     */
    public XmlBasedDocument(Document xmlDocument) {
        this.xmlDocument = xmlDocument;
    }

    /**
     * Give the DOM
     * 
     * @return
     */
    public Document getXmlDocument() {
        return xmlDocument;
    }

    /**
     * Helper that gives the list of element from an xPath query
     * 
     * @param xPathQuery
     * @return
     * @throws SilecsException
     */
    public List<Element> getElements(String xPathQuery) throws SilecsException {
        return getElements(xmlDocument, xPathQuery);
    }

    /**
     * Helper that gives the list of element from an xPath query
     * 
     * @param node       node from where the query is executed
     * @param xPathQuery
     * @return
     * @throws SilecsException
     */
    public List<Element> getElements(Node node, String xPathQuery) throws SilecsException {
        List<Node> nodes = getNodes(node, xPathQuery);
        List<Element> elements = new ArrayList<Element>(nodes.size());

        for (Node n : nodes) {
            if (n instanceof Element) {
                elements.add((Element) n);
            }
        }

        return elements;
    }

    /**
     * Helper that gives the first element from an xPath query
     * 
     * @param xPathQuery
     * @return
     * @throws SilecsException
     */
    public Element getSingleElement(String xPathQuery) throws SilecsException {
        Node foundNode = getSingleNode(xPathQuery);
        if (foundNode instanceof Element) {
            return (Element) foundNode;
        } else {
            return null;
        }
    }

    public List<Node> getNodes(String xPathQuery) throws SilecsException {
        return getNodes(xmlDocument, xPathQuery);
    }

    public List<Node> getNodes(Node node, String xPathQuery) throws SilecsException {
        if (xPathQuery == null || xPathQuery.isEmpty()) {
            return Collections.emptyList();
        }

        // check the node is from the document
        if (node != xmlDocument && node.getOwnerDocument() != xmlDocument) {
            throw new SilecsException("The given node is not part of the document being queried");
        }

        try {
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile(xPathQuery);
            NodeList foundNodes = (NodeList) expr.evaluate(xmlDocument, XPathConstants.NODESET);
            List<Node> nodes = new ArrayList<Node>(foundNodes.getLength());
            for (int i = 0; i < foundNodes.getLength(); i++) {
                nodes.add(foundNodes.item(i));
            }

            return nodes;
        } catch (XPathExpressionException e) {
            throw new SilecsException("Could not get entity's node using xpath " + xPathQuery, e);
        }
    }

    /**
     * Helper that gives the first node from an xPath query
     * 
     * @param xPathQuery
     * @return
     * @throws SilecsException
     * @throws XPathExpressionException
     */
    public Node getSingleNode(String xPathQuery) throws SilecsException {
        if (xPathQuery == null || xPathQuery.isEmpty()) {
            return null;
        }

        try {
            XPathFactory xPathfactory = XPathFactory.newInstance();
            XPath xpath = xPathfactory.newXPath();
            XPathExpression expr = xpath.compile(xPathQuery);
            return (Node) expr.evaluate(xmlDocument, XPathConstants.NODE);
        } catch (XPathExpressionException e) {
            throw new SilecsException("Could not get entity's node using xpath " + xPathQuery, e);
        }
    }

    /**
     * Helper that gives the tag name of the elements matched by the xpath
     * 
     * @param xPathQuery
     * @return
     * @throws SilecsException
     */
    public List<String> getTagNames(String xPathQuery) throws SilecsException {
        List<Element> elements = getElements(xPathQuery);
        List<String> nodeNames = new ArrayList<String>(elements.size());

        for (Element e : elements) {
            nodeNames.add(e.getTagName());
        }

        return nodeNames;
    }

    /**
     * Give the string representation of the DOM
     */
    @Override
    public String toString() {
        try {
            return XmlUtils.nodeToString(xmlDocument);
        } catch (TransformerException e) {
            ConsoleHandler.printStackTrace(e);
            return super.toString();
        } catch (TransformerFactoryConfigurationError e) {
            ConsoleHandler.printStackTrace(e.getException());
            return super.toString();
        }
    }
}