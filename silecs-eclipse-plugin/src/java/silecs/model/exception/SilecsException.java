// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.model.exception;

public class SilecsException extends Exception {

    private static final long serialVersionUID = 5282474727123991544L;

    /**
     * The error code which can be used to localize the error messages
     */
    private final int errorCode;

    /** Empty constructor **/
    public SilecsException() {
        this.errorCode = -1;
    }

    /**
     * Constructor with the Silecs error code
     * 
     * @param errorCode
     */
    public SilecsException(int errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Constructor
     * 
     * @param message
     */
    public SilecsException(String message) {
        super(message);
        this.errorCode = -1;
    }

    /**
     * Constructor with the Silecs error code
     * 
     * @param errorCode
     * @param message
     */
    public SilecsException(int errorCode, String message) {
        super(message);
        this.errorCode = errorCode;
    }

    /**
     * Constructor
     * 
     * @param message
     * @param cause   previous cause of the error (exception chaining)
     */
    public SilecsException(String message, Throwable cause) {
        super(message, cause);
        this.errorCode = -1;
    }

    /**
     * Constructor
     * 
     * @param errorCode
     * @param message
     * @param cause
     */
    public SilecsException(int errorCode, String message, Throwable cause) {
        super(message, cause);
        this.errorCode = errorCode;
    }

    /**
     * Returns the Silecs error code -1 is the default value if no error code is set
     * 
     * @return
     */
    public int getErrorCode() {
        return errorCode;
    }
}
