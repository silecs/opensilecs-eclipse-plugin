// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.wizards;

import java.io.File;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.wizard.Wizard;
import org.eclipse.ui.INewWizard;
import org.eclipse.ui.IWorkbench;

import silecs.control.core.DeployProjectNature;
import silecs.model.exception.SilecsException;
import silecs.utils.SilecsConstants;
import silecs.utils.OSExecute;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;
import silecs.utils.XmlUtils;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;
import silecs.view.wizards.page.SilecsProjectPage;

public class NewSilecsDeployWizard extends Wizard implements INewWizard {

    SilecsProjectPage page;

    public NewSilecsDeployWizard() {
        super();
        setWindowTitle(SilecsConstants.NEW_DEPLOY_WIZARD_TITLE);
    }

    @Override
    public void addPages() {
        super.addPages();
        page = new SilecsProjectPage(SilecsConstants.NEW_DEPLOY_PAGE_TITLE, SilecsConstants.ProjectType.DEPLOY_PROJECT,
                "Enter deploy-unit name", "Enter silecs version");
        addPage(page);
    }

    @Override
    public boolean performFinish() {
        try {
            ConsoleHandler.clear();
            String deployName = page.getprojectNameText();
            Version silecsVersion = page.getsilecsVersion();
            String deploySchema = MainPreferencePage.getModelBasePath(silecsVersion) + "/"
                    + SilecsConstants.DEPLOY_SCHEMA_XSD;
            if (!new File(deploySchema).exists()) {
                throw new SilecsException("The Deploy Schema: '" + deploySchema
                        + "' does not exist. Please check your Eclipse-Silecs preferences!");
            }
            IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
            String workspacePath = workspaceRoot.getLocation().toFile().getPath();

            IProject project = workspaceRoot.getProject(deployName);
            OSExecute.executePython("iefiles", "newDeployProject", silecsVersion,
                    new String[] { workspacePath, deployName, deploySchema, silecsVersion.toString() });
            XmlUtils.reloadDependencies();

            IFile deployFile = SilecsUtils.getSilecsDeployFile(project);

            IProgressMonitor monitor = new NullProgressMonitor();
            // project.create(monitor);
            // DeployProjectNature.addDeployNature(project, monitor); TODO: No idea why this
            // does not work

            IProjectDescription description = project.getWorkspace().newProjectDescription(project.getName());
            String[] newNatures = new String[1];
            newNatures[0] = DeployProjectNature.NATURE_ID;
            description.setNatureIds(newNatures);

            project.create(description, null);

            project.open(IResource.FORCE, monitor);

            SilecsUtils.openInEditor(deployFile);
        } catch (Exception e) {
            e.printStackTrace();
            ConsoleHandler.printStackTrace(e);
        }
        return true;
    }

    @Override
    public void init(IWorkbench arg0, IStructuredSelection arg1) {
        // TODO Auto-generated method stub

    }
}
