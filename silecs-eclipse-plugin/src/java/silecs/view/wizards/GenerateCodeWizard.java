// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.wizards;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.jface.wizard.Wizard;

import silecs.view.wizards.page.PickTemplatePage;

public class GenerateCodeWizard extends Wizard {
    private File[] templates;
    File choice = null;
    PickTemplatePage page;

    public GenerateCodeWizard(File[] templateFiles) {
        super();
        setWindowTitle("Pick a template");
        templates = templateFiles;
    }

    @Override
    public void addPages() {
        super.addPages();

        List<String> templateNames = new ArrayList<String>();
        for (File file : templates) {
            templateNames.add(file.getName());
        }
        String[] simpleArray = new String[templateNames.size()];
        page = new PickTemplatePage("Pick a template", templateNames.toArray(simpleArray));
        addPage(page);
    }

    public File getChoice() {
        return choice;
    }

    @Override
    public boolean performFinish() {
        String template = page.getTemplate();
        // ConsoleHandler.printMessage("template selected: " + template);
        for (File file : templates) {
            // ConsoleHandler.printMessage("file available: " + file.getName());
            if (file.getName().equals(template))
                choice = file;
        }
        // ConsoleHandler.printMessage("choice full path: " + choice.getAbsolutePath());
        return true;
    }
}
