// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.wizards.page;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;

public class PickTemplatePage extends WizardPage {

    private String[] templates;
    private Composite container;
    private Combo templatePicker;

    /**
     * @param pageName
     */
    public PickTemplatePage(String pageName, String[] templateNames) {
        super(pageName);
        setTitle(pageName);
        templates = templateNames;
    }

    @Override
    public void createControl(Composite parent) {
        container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 1;

        templatePicker = new Combo(container, SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
        templatePicker.setItems(templates);
        templatePicker.select(0);
        GridData dataLayout = new GridData();
        dataLayout.horizontalSpan = 1;
        templatePicker.setLayoutData(dataLayout);
        setControl(container);
    }

    public String getTemplate() {
        return templatePicker.getText();
    }
}
