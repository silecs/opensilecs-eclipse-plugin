// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.wizards.page;

import java.util.List;

import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;

import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

import silecs.utils.SilecsConstants;
import silecs.utils.SilecsUtils;

public class PickVersionPage extends WizardPage {

    private Combo silecsVersionText;
    private String silecsVersionDescription;

    public PickVersionPage(String pageName) {
        super(pageName);
        setTitle(pageName);
        silecsVersionDescription = "New silecs version";
    }

    @Override
    public void createControl(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
        GridLayout layout = new GridLayout();
        container.setLayout(layout);
        layout.numColumns = 2;

        Label classVersionLabel = new Label(container, SWT.NONE);
        classVersionLabel.setText(silecsVersionDescription);

        silecsVersionText = new Combo(container, SWT.BORDER | SWT.V_SCROLL | SWT.READ_ONLY);
        List<String> availableSilecsVersions = SilecsUtils.readOutAvailableSilecsVersions();
        availableSilecsVersions.addAll(SilecsConstants.ADDITIONAL_SILECS_TARGET_MIGRATION_VERSIONS);
        silecsVersionText.setItems(availableSilecsVersions.toArray(new String[availableSilecsVersions.size()]));
        silecsVersionText.select(SilecsConstants.DEFAULT_SILECS_VERSION_INDEX);
        GridData dataLayout = new GridData();
        dataLayout.horizontalSpan = 2;
        silecsVersionText.setLayoutData(dataLayout);

        // required to avoid an error in the system
        setControl(container);
        setPageComplete(true);
    }

    public String getsilecsVersionText() {
        return silecsVersionText.getText();
    }
}
