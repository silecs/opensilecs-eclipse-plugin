// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.marker;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.wst.xml.core.internal.document.NodeImpl;

import silecs.model.document.SilecsDocumentError;

@SuppressWarnings("restriction")
public abstract class SilecsMarker {

//    private static final Logger LOGGER = LoggerFactory.getLogger(SilecsMarker.class);

    public static final String TYPE = "silecs.view.marker";

    public static final String NODE_ATTR = "node";

    public static IMarker create(IFile file, SilecsDocumentError error, NodeImpl node) throws CoreException {
        IMarker marker = create(file, error);
        if (node != null) {
            marker.setAttribute(IMarker.CHAR_START, node.getStartOffset());
            marker.setAttribute(IMarker.CHAR_END, node.getEndOffset());
            marker.setAttribute(NODE_ATTR, node);
        }

        return marker;
    }

    public static IMarker create(IFile file, SilecsDocumentError error) throws CoreException {
        IMarker marker = file.createMarker(SilecsMarker.TYPE);

        marker.setAttribute(IMarker.MESSAGE, error.message);
        marker.setAttribute(IMarker.LINE_NUMBER, error.lineNumber);
        switch (error.level) {
        case INFO:
            marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_INFO);
            break;
        case WARNING:
            marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_WARNING);
            break;
        default:
            marker.setAttribute(IMarker.SEVERITY, IMarker.SEVERITY_ERROR);
            break;
        }

        return marker;
    }
}
