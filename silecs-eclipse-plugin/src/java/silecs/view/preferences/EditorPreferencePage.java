// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.preferences;

import org.eclipse.jface.preference.PreferencePage;
import org.eclipse.swt.SWT;

import org.eclipse.swt.layout.GridLayout;

import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;

import silecs.activator.Activator;

public class EditorPreferencePage extends PreferencePage implements IWorkbenchPreferencePage {

//    // validate on save
//    public static final String VALIDATE_ON_SAVE = "VALIDATE_ON_SAVE";
//    
//    // colors
//    public static final String EDITABLE_COLOR_R = "XML_EDITOR_COLOR_EDITABLE_R";
//    public static final String EDITABLE_COLOR_G = "XML_EDITOR_COLOR_EDITABLE_G";
//    public static final String EDITABLE_COLOR_B = "XML_EDITOR_COLOR_EDITABLE_B";
//
//    public static final String CONTENT_COLOR_R = "XML_EDITOR_COLOR_CONTENT_R";
//    public static final String CONTENT_COLOR_G = "XML_EDITOR_COLOR_CONTENT_G";
//    public static final String CONTENT_COLOR_B = "XML_EDITOR_COLOR_CONTENT_B";
//    
//    // link documentation to the editor
//    public static final String LINK_DOCUMENTATION = "DISPLAY_DOCUMENTATION";
//    public static final String BRING_DOCUMENTATION_TO_FRONT = "DOCUMENTATION_TO_FRONT";
//    
//    public static final String SHOW_GENERATED = "SHOW_GENERATED"; 
//
//    // default theme
//    public static final RGB DEFAULT_EDITABLE = new RGB(0, 0, 0);
//    public static final RGB DEFAULT_CONTENT = new RGB(130, 170, 150);
//
//    // ocean theme
//    public static final RGB OCEAN_EDITABLE = new RGB(0, 0, 128);
//    public static final RGB OCEAN_CONTENT = new RGB(0, 206, 209);
//
//    // fire theme
//    public static final RGB FIRE_EDITABLE = new RGB(205, 79, 57);
//    public static final RGB FIRE_CONTENT = new RGB(205, 205, 0);
//
//    private Button validationOnSave;
//    
//    private ColorSelector editableColorSelector;
//    private ColorSelector contentColorSelector;
//    
//    private Button linkDocumentation;
//    private Button bringDocumentationToFront;
//    private Button showGeneratedContent;

    @Override
    public void init(IWorkbench workbench) {
        setPreferenceStore(Activator.getDefault().getPreferenceStore());
        setDescription("Not yet available");
    }

    @Override
    protected Control createContents(Composite parent) {
        Composite container = new Composite(parent, SWT.NONE);
//
//        IPreferenceStore prefs = getPreferenceStore();
//
        GridLayout layout = new GridLayout(1, false);
        container.setLayout(layout);
//        
//        // validation options
//        Group validationGroup = new Group(container, SWT.NONE);
//        validationGroup.setText("Validation");
//        validationGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
//        layout = new GridLayout(1, false);
//        validationGroup.setLayout(layout);
//        validationOnSave = new Button(validationGroup, SWT.CHECK);
//        validationOnSave.setText("Validate after Safe");
//        validationOnSave.setSelection(prefs.getBoolean(VALIDATE_ON_SAVE));
//
//        // color options
//        Group colorGroup = new Group(container, SWT.NONE);
//        colorGroup.setText("Colors");
//        colorGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
//        layout = new GridLayout(2, false);
//        colorGroup.setLayout(layout);
//        
//        Label editableColorLabel = new Label(colorGroup, SWT.NONE);
//        editableColorLabel.setText("Editable Text color");
//        
//        contentColorSelector = new ColorSelector(colorGroup);
//        RGB contentColor = new RGB(prefs.getInt(CONTENT_COLOR_R), prefs.getInt(CONTENT_COLOR_G),
//                prefs.getInt(CONTENT_COLOR_B));
//        contentColorSelector.setColorValue(contentColor);
//
//        Label contentColorLabel = new Label(colorGroup, SWT.NONE);
//        contentColorLabel.setText("Content Text Color");
//        
//        editableColorSelector = new ColorSelector(colorGroup);
//        RGB editableColor = new RGB(prefs.getInt(EDITABLE_COLOR_R), prefs.getInt(EDITABLE_COLOR_G),
//                prefs.getInt(EDITABLE_COLOR_B));
//        editableColorSelector.setColorValue(editableColor);
//
//        Group buttonGroup = new Group(colorGroup, SWT.NONE);
//        buttonGroup.setText("Color Scheme");
//        RowLayout buttonLayout = new RowLayout(SWT.HORIZONTAL);
//        buttonLayout.marginLeft = 5;
//        buttonLayout.marginRight = 5;
//        buttonLayout.marginTop = 5;
//        buttonLayout.marginBottom = 5;
//        buttonLayout.spacing = 5;
//        buttonLayout.pack = false;
//        buttonLayout.justify = true;
//        buttonGroup.setLayout(buttonLayout);
//        GridData buttonGridData = new GridData(SWT.FILL, SWT.FILL, true, false);
//        buttonGridData.horizontalSpan = 2;
//        buttonGroup.setLayoutData(buttonGridData);
//        
//        
//        Button defaultScheme = new Button(buttonGroup, SWT.PUSH);
//        defaultScheme.setText("default");
//        
//        Button oceanScheme = new Button(buttonGroup, SWT.PUSH);
//        oceanScheme.setText("ocean");
//        
//        Button fireScheme = new Button(buttonGroup, SWT.PUSH);
//        fireScheme.setText("fire");
//
//        defaultScheme.addListener(SWT.Selection, new Listener() {
//            
//            @Override
//            public void handleEvent(Event event) {
//                contentColorSelector.setColorValue(DEFAULT_CONTENT);
//                editableColorSelector.setColorValue(DEFAULT_EDITABLE);
//            }
//        });
//        
//        oceanScheme.addListener(SWT.Selection, new Listener() {
//            
//            @Override
//            public void handleEvent(Event event) {
//                contentColorSelector.setColorValue(OCEAN_CONTENT);
//                editableColorSelector.setColorValue(OCEAN_EDITABLE);
//            }
//        });
//        
//        fireScheme.addListener(SWT.Selection, new Listener() {
//            
//            @Override
//            public void handleEvent(Event event) {
//                contentColorSelector.setColorValue(FIRE_CONTENT);
//                editableColorSelector.setColorValue(FIRE_EDITABLE);
//            }
//        });
//        
//        Group others = new Group(container, SWT.NONE);
//        others.setText("Others");
//        others.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false));
//        layout = new GridLayout(1, false);
//        others.setLayout(layout);
//        linkDocumentation = new Button(others, SWT.CHECK);
//        linkDocumentation.setText("Link Documentation");
//        linkDocumentation.setSelection(prefs.getBoolean(LINK_DOCUMENTATION));
//        bringDocumentationToFront = new Button(others, SWT.CHECK);
//        bringDocumentationToFront.setText("Bring Documentation to Front");
//        bringDocumentationToFront.setSelection(prefs.getBoolean(BRING_DOCUMENTATION_TO_FRONT));
//        
//        showGeneratedContent = new Button(others, SWT.CHECK);
//        showGeneratedContent.setText("Show generated content");
//        showGeneratedContent.setSelection(prefs.getBoolean(SHOW_GENERATED));
//        
        return container;
    }

//    
    @Override
    public boolean performOk() {
//        IPreferenceStore prefs = getPreferenceStore();
//        
//        RGB content = contentColorSelector.getColorValue();
//        RGB editable = editableColorSelector.getColorValue();
//        
//        prefs.setValue(VALIDATE_ON_SAVE, validationOnSave.getSelection());
//        
//        prefs.setValue(CONTENT_COLOR_R, content.red);
//        prefs.setValue(CONTENT_COLOR_G, content.green);
//        prefs.setValue(CONTENT_COLOR_B, content.blue);
//        prefs.setValue(EDITABLE_COLOR_R, editable.red);
//        prefs.setValue(EDITABLE_COLOR_G, editable.green);
//        prefs.setValue(EDITABLE_COLOR_B, editable.blue);
//        
//        prefs.setValue(LINK_DOCUMENTATION, linkDocumentation.getSelection());
//        prefs.setValue(BRING_DOCUMENTATION_TO_FRONT, bringDocumentationToFront.getSelection());
//        prefs.setValue(SHOW_GENERATED, showGeneratedContent.getSelection());
//        
        return true;
    }

    @Override
    protected void performDefaults() {
//        validationOnSave.setSelection(true);
//        
//        contentColorSelector.setColorValue(DEFAULT_CONTENT);
//        editableColorSelector.setColorValue(DEFAULT_EDITABLE);
//        
//        linkDocumentation.setSelection(true);
//        bringDocumentationToFront.setSelection(false);
//        showGeneratedContent.setSelection(false);
    }
}
