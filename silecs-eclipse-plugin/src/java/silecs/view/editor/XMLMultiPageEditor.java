// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.editor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.ResourcesPlugin;

import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.IRegion;
import org.eclipse.jface.text.ITextViewer;
import org.eclipse.swt.layout.GridData;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wst.sse.ui.StructuredTextEditor;
import org.eclipse.wst.sse.ui.internal.contentassist.ContentAssistUtils;
import org.eclipse.wst.xml.core.internal.document.NodeImpl;
import org.eclipse.wst.xml.ui.internal.XMLUIPlugin;
import org.eclipse.wst.xml.ui.internal.tabletree.IDesignViewer;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLMultiPageEditorPart;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLTableTreeHelpContextIds;
import org.eclipse.core.runtime.IProgressMonitor;

import silecs.view.console.ConsoleHandler;
import silecs.view.editor.internal.SilecsXMLTableTreeViewer;
import silecs.view.job.ValidationJob;

@SuppressWarnings("restriction")
public class XMLMultiPageEditor extends XMLMultiPageEditorPart {

    SilecsXMLTableTreeViewer xmlTableTreeViewer;

    public XMLMultiPageEditor() {
        super();
    }

    @Override
    public void init(IEditorSite site, IEditorInput input) throws PartInitException {
        super.init(site, input);

        // useless to instantiate toolbar since not used
        noToolbar();
    }

    @Override
    public void dispose() {
        ResourcesPlugin.getWorkspace().removeResourceChangeListener(xmlTableTreeViewer);
        super.dispose();
    }

    @Override
    protected void createPages() {
        super.createPages();

        // add itself as resource change listener
        ResourcesPlugin.getWorkspace().addResourceChangeListener(xmlTableTreeViewer, IResourceChangeEvent.POST_CHANGE);
    }

    @Override
    protected IDesignViewer createDesignPage() {
        xmlTableTreeViewer = new SilecsXMLTableTreeViewer(getContainer(), this);
        xmlTableTreeViewer.getControl().setLayoutData(new GridData(GridData.FILL_BOTH));

        // Set the default info-pop for XML design viewer.
        XMLUIPlugin.getInstance().getWorkbench().getHelpSystem().setHelp(xmlTableTreeViewer.getControl(),
                XMLTableTreeHelpContextIds.XML_DESIGN_VIEW_HELPID);

        return xmlTableTreeViewer;
    }

    public IDocument getDocument() {
        IDocument document = null;
        StructuredTextEditor textEditor = (StructuredTextEditor) getAdapter(StructuredTextEditor.class);
        if (textEditor != null) {
            document = textEditor.getDocumentProvider().getDocument(textEditor.getEditorInput());
        }

        return document;
    }

    public static XMLMultiPageEditor getOpenedEditor(IFile file) {

        // get all the opened editors
        List<IEditorReference> editors = new ArrayList<>();
        for (IWorkbenchWindow window : PlatformUI.getWorkbench().getWorkbenchWindows()) {
            for (IWorkbenchPage page : window.getPages()) {
                editors.addAll(Arrays.asList(page.getEditorReferences()));
            }
        }

        for (IEditorReference er : editors) {
            IEditorPart editor = er.getEditor(false);
            try {
                IEditorInput editorInput = er.getEditorInput();
                if (editor instanceof XMLMultiPageEditor && editorInput instanceof IFileEditorInput) {
                    IFileEditorInput input = (IFileEditorInput) editorInput;

                    if (input.getFile().equals(file)) {
                        return (XMLMultiPageEditor) editor;
                    }
                }
            } catch (PartInitException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public NodeImpl getNodeAtPosition(int line, int column) {
        try {
            // line 1 is considered as line 0 by eclipse
            // same for columns
            if (line > 0) {
                IRegion region = getDocument().getLineInformation(line - 1);
                ITextViewer viewer = ((StructuredTextEditor) getAdapter(StructuredTextEditor.class)).getTextViewer();
                return (NodeImpl) ContentAssistUtils.getNodeAt(viewer, region.getOffset() + column - 2);
            }
        } catch (BadLocationException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void doSave(IProgressMonitor monitor) {
        IEditorInput editorInput = getEditorInput();
        if (editorInput instanceof IFileEditorInput) {
            IFile fesaFile = ((IFileEditorInput) editorInput).getFile();

            super.doSave(monitor);

            ConsoleHandler.printMessage("Validating XML  ...");
            ValidationJob job;
            job = new ValidationJob(fesaFile);
            job.schedule();
        }
    }

}
