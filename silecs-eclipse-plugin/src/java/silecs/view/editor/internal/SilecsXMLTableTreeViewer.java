// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.editor.internal;

import java.util.Arrays;
import java.util.Comparator;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IMarkerDelta;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.wst.xml.core.internal.document.NodeImpl;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLTableTreeContentProvider;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLTableTreeViewer;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import silecs.view.console.ConsoleHandler;
import silecs.view.marker.SilecsMarker;

@SuppressWarnings("restriction")
public class SilecsXMLTableTreeViewer extends XMLTableTreeViewer implements IResourceChangeListener {

    private final IEditorPart parentEditor;
    private final SilecsTableColumnViewerLabelProvider labelProvider;

    /**
     * @param parent
     */
    public SilecsXMLTableTreeViewer(Composite parent, IEditorPart parentEditor) {
        super(parent);
        this.parentEditor = parentEditor;

        this.propertyDescriptorFactory = new SilecsXMLTableTreePropertyDescriptorFactory();
        XMLTableTreeContentProvider provider = new SilecsXMLTableTreeContentProvider();
        setContentProvider(provider);
        this.labelProvider = new SilecsTableColumnViewerLabelProvider(provider);
        setLabelProvider(labelProvider);
        ColumnViewerToolTipSupport.enableFor(this);

        // Dirty hack to fix bug in eclipse-neon:
        // https://bugs.eclipse.org/bugs/show_bug.cgi?id=517360
        Point size = getControl().getSize();
        size.x = 1000;
        getControl().setSize(size);
    }

    @Override
    protected void inputChanged(Object input, Object oldInput) {
        super.inputChanged(input, oldInput);

        labelProvider.clearFaults();
        IEditorInput editorInput = parentEditor.getEditorInput();
        if (editorInput instanceof IFileEditorInput) {
            try {
                IFile inputFile = ((IFileEditorInput) editorInput).getFile();
                IMarker[] markers = inputFile.findMarkers(SilecsMarker.TYPE, false, IResource.DEPTH_ZERO);

                for (IMarker m : markers) {
                    displayMarker(m);
                }
            } catch (CoreException e) {
                ConsoleHandler.printStackTrace(e);
            }
        }

        refresh();
    }

    @Override
    public void refresh(Object element, boolean updateLabels) {
        super.refresh(element, updateLabels);

        // we also want to refresh label of parents elements
        if (element instanceof Element) {
            Node parent = ((Element) element).getParentNode();
            while (parent != null) {
                super.refresh(parent, updateLabels);
                parent = parent.getParentNode();
            }
        }
    }

    @Override
    public void resourceChanged(IResourceChangeEvent event) {
        IEditorInput editorInput = parentEditor.getEditorInput();
        if (editorInput instanceof IFileEditorInput) {
            // only apply changes if input is a file
            final IFile inputFile = ((IFileEditorInput) editorInput).getFile();

            // sort the markers so that we handle first the deletion and the the creation
            final IMarkerDelta makerDelta[] = event.findMarkerDeltas(SilecsMarker.TYPE, false);
            Arrays.sort(makerDelta, new Comparator<IMarkerDelta>() {
                @Override
                public int compare(IMarkerDelta md1, IMarkerDelta md2) {
                    if (md1.getKind() == IResourceDelta.ADDED) {
                        return md2.getKind() == IResourceDelta.ADDED ? 0 : 1;
                    } else if (md1.getKind() == IResourceDelta.REMOVED) {
                        return md2.getKind() == IResourceDelta.REMOVED ? 0 : -1;
                    } else if (md2.getKind() == IResourceDelta.ADDED) {
                        return -1;
                    } else if (md2.getKind() == IResourceDelta.REMOVED) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            });

            getControl().getDisplay().syncExec(new Runnable() {
                @Override
                public void run() {
                    // this is a hack to avoid automatic scrolling to
                    // one of the faulty nodes
                    TreeItem item = getTree().getTopItem();
                    for (IMarkerDelta md : makerDelta) {
                        if (md.getResource().equals(inputFile)) {
                            if (md.getKind() == IResourceDelta.ADDED) {
                                displayMarker(md.getMarker());
                            } else if (md.getKind() == IResourceDelta.REMOVED) {
                                removeMarker(md.getMarker());
                            }
                        }
                    }

                    if (item != null) {
                        getTree().setTopItem(item);
                    }
                }
            });
        }
    }

    public void displayMarker(final IMarker marker) {
        NodeImpl node = null;
        try {
            node = (NodeImpl) marker.getAttribute(SilecsMarker.NODE_ATTR);
            if (node != null) {
                labelProvider.addFault(marker, node);
                reveal(node);
            }
        } catch (CoreException e) {
            // LOGGER.error("Could not highlight node: {}", e.getMessage());
        }
    }

    public void removeMarker(final IMarker marker) {
        labelProvider.removeFault(marker);
    }

    @Override
    public String getTitle() {
        return "Tree View";
    }

}
