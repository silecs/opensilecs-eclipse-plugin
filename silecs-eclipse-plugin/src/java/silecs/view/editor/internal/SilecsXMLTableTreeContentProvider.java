// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.editor.internal;

import org.eclipse.osgi.util.TextProcessor;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLTableTreeContentProvider;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import silecs.utils.SilecsConstants;

@SuppressWarnings("restriction")
public class SilecsXMLTableTreeContentProvider extends XMLTableTreeContentProvider {

    @Override
    public String getColumnText(Object object, int column) {
        String result = super.getColumnText(object, column);

        if ((result.isEmpty() || result.startsWith("(")) && object instanceof Element && column == 1) {
            result = getElementValueHelper((Element) object);
        }

        result = TextProcessor.process(result);
        return result != null ? result : "";
    }

    private String getElementValueHelper(Element element) {

        StringBuilder sb = new StringBuilder();
        if (element.hasAttributes()) {
            NamedNodeMap map = element.getAttributes();

            sb.append("(");
            // put name first
            Attr name = (Attr) map.getNamedItem(SilecsConstants.NAME);
            if (name != null) {
                sb.append(name.getValue() + ": ");
            }

            // put other attributes
            for (int i = 0; i < map.getLength(); i++) {
                Attr a = (Attr) map.item(i);
                String attrName = a.getName();

                // name has already been added
                // do not propagate id
                if (!(attrName.equals(SilecsConstants.NAME))) {
                    sb.append(attrName + "=" + a.getValue() + ", ");
                }
            }

        }

        if (sb.length() < 2) {
            // if we didn't find smth interesting
            // the node behaves like a container
            return getContainerElementValueHelper(element);
        } else if (sb.charAt(0) == '(') {
            // remove the last comma/colon and close the parenthesis
            sb.setLength(sb.length() - 2);
            sb.append(")");
        }

        return sb.toString();
    }

    private String getContainerElementValueHelper(Element element) {
        if (element.hasAttribute(SilecsConstants.NAME)) {
            return element.getAttribute(SilecsConstants.NAME);
        } else if (element.hasChildNodes()) {
            NodeList nodeList = element.getChildNodes();

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < nodeList.getLength(); i++) {
                Node n = nodeList.item(i);
                if (n instanceof Element) {
                    String val = getContainerElementValueHelper((Element) n);
                    if (!val.isEmpty()) {
                        if (sb.length() != 0) {
                            sb.append(", ");
                        }

                        sb.append(val);
                    }

                }
            }

            return sb.toString();
        } else {
            return "";
        }
    }

}
