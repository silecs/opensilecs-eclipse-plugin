// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.editor.internal;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.ui.views.properties.IPropertyDescriptor;
import org.eclipse.wst.xml.core.internal.contentmodel.CMNode;
import org.eclipse.wst.xml.ui.internal.properties.EnumeratedStringPropertyDescriptor;
import org.eclipse.wst.xml.ui.internal.tabletree.XMLTableTreePropertyDescriptorFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import silecs.model.exception.SilecsException;
import silecs.utils.SilecsConstants;
import silecs.view.console.ConsoleHandler;

@SuppressWarnings("restriction")
class SilecsXMLTableTreePropertyDescriptorFactory extends XMLTableTreePropertyDescriptorFactory {

    public static final String DESIGN_NAME_MATCHER = "silecs-design-name";
    public static final String DESIGN_REF_MATCHER = "silecs-design-ref";
    public static final String DESIGN_VERSION_MATCHER = "silecs-design-version";

    @Override
    protected IPropertyDescriptor createPropertyDescriptorHelper(String name, Element element, CMNode cmNode) {
        Collection<String> values = null;
        try {
            if (isSilecsDesignName(element, name)) {

                values = getPossibleClassNames();
            }
            if (isSilecsDesignRef(element, name)) {

                values = getPossibleClassRefNames(element);
            }

            if (isSilecsDesignVersion(element, name)) {
                if (!element.getAttribute(DESIGN_NAME_MATCHER).isEmpty()) {
                    values = getPossibleClassVersions(element.getAttribute(DESIGN_NAME_MATCHER));
                }
            }
        } catch (SilecsException e) {
            ConsoleHandler.printStackTrace(e);
        }

        if (values != null && !values.isEmpty()) {
            return new EnumeratedStringPropertyDescriptor(name, name, values.toArray(new String[values.size()]));
        } else {
            return super.createPropertyDescriptorHelper(name, element, cmNode);
        }
    }

    private static boolean isSilecsDesignRef(Element element, String attribute) {
        if (element.getTagName().matches("Device") && attribute.matches(DESIGN_REF_MATCHER))
            return true;
        return false;
    }

    private static boolean isSilecsDesignName(Element element, String attribute) {
        if (element.getTagName().matches("SilecsDesign") && attribute.matches(DESIGN_NAME_MATCHER))
            return true;
        return false;
    }

    private static boolean isSilecsDesignVersion(Element element, String attribute) {
        if (element.getTagName().matches("SilecsDesign") && attribute.matches(DESIGN_VERSION_MATCHER))
            return true;
        return false;
    }

    private static List<String> getPossibleClassRefNames(Element element) throws SilecsException {
        Document xmlDoc = element.getOwnerDocument();
        NodeList silecsDesignNodes = xmlDoc.getElementsByTagName("SilecsDesign");
        List<String> designNames = new ArrayList<>();
        for (int i = 0; i < silecsDesignNodes.getLength(); i++) {
            Element silecsDesign = (Element) silecsDesignNodes.item(i);
            if (silecsDesign.hasAttribute("silecs-design-name")) {
                designNames.add(silecsDesign.getAttribute("silecs-design-name"));
            }
        }
        return designNames;
    }

    private static List<String> getPossibleClassNames() throws SilecsException {
        Set<File> classes = getClassFiles();
        String searchTerm = "." + SilecsConstants.IEDESIGN_FILE;
        List<String> classNames = new ArrayList<>();

        for (File classFile : classes) {
            String className = classFile.getName().replace(searchTerm, "");
            classNames.add(className);
        }
        return classNames;
    }

    private static List<String> getPossibleClassVersions(String className) throws SilecsException {
        Set<File> classes = getClassFiles();
        List<String> classVersions = new ArrayList<>();
        String searchTerm = "." + SilecsConstants.IEDESIGN_FILE;

        for (File classFile : classes) {
            String classNameFile = classFile.getName().replace(searchTerm, "");
            // ConsoleHandler.printMessage("getPossibleClassVersions - classNameFile: " +
            // classNameFile);
            // ConsoleHandler.printMessage("getPossibleClassVersions - className: " +
            // className);
            if (classNameFile.equals(className)) {
                try {
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(classFile);
                    Node classNode = doc.getElementsByTagName("SILECS-Class").item(0);
                    if (classNode != null) {
                        classVersions.add(classNode.getAttributes().getNamedItem("version").getNodeValue());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    ConsoleHandler.printError("No version found in class: " + classNameFile);
                }
            }
        }
        return classVersions;
    }

    private static Set<File> getClassFiles() throws SilecsException {
        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        Set<File> projects = listDirectories(workspaceRoot.getLocation().toFile());
        Set<File> classes = new HashSet<File>();

        String searchTerm = "." + SilecsConstants.IEDESIGN_FILE;
        for (File p : projects) {
            File designFolder = new File(p.getAbsolutePath() + "/src");
            if (!designFolder.exists())
                continue;

            File[] listOfFiles = designFolder.listFiles();
            for (int i = 0; i < listOfFiles.length; i++) {
                if (listOfFiles[i].getName().endsWith(searchTerm)) {
                    classes.add(listOfFiles[i]);
                }
            }
        }
        return classes;
    }

    private static Set<File> listDirectories(File directory) {
        File[] directories = directory.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return pathname.isDirectory();
            }
        });

        if (directories != null) {
            return new TreeSet<File>(Arrays.asList(directories));
        } else {
            return Collections.emptySet();
        }
    }
}
