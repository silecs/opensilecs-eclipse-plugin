// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.editor.internal;

import java.util.Collections;
import java.util.List;

import org.eclipse.wst.xml.core.internal.contentmodel.CMElementDeclaration;
import org.eclipse.wst.xml.core.internal.document.CMNodeUtil;
import org.eclipse.wst.xsd.contentmodel.internal.XSDImpl.XSDElementDeclarationAdapter;
import org.eclipse.xsd.XSDElementDeclaration;
import org.eclipse.xsd.XSDIdentityConstraintDefinition;
import org.w3c.dom.Element;

@SuppressWarnings("restriction")
public abstract class SchemaHelper {

    public static List<XSDIdentityConstraintDefinition> findIdentityConstraints(Element element) {
        CMElementDeclaration elemDecl = CMNodeUtil.getElementDeclaration(element);
        if (elemDecl instanceof XSDElementDeclarationAdapter) {
            XSDElementDeclarationAdapter xsdElemAdapt = (XSDElementDeclarationAdapter) elemDecl;
            XSDElementDeclaration xsdElementDecl = (XSDElementDeclaration) xsdElemAdapt.getKey();

            // get all the key constraints definitions
            List<XSDIdentityConstraintDefinition> constraints = xsdElementDecl.getResolvedElementDeclaration()
                    .getIdentityConstraintDefinitions();
            constraints.addAll(xsdElementDecl.getResolvedElementDeclaration().getIdentityConstraintDefinitions());

            return constraints;
        } else {
            return Collections.emptyList();
        }
    }

    public static XSDIdentityConstraintDefinition findIdentityConstraint(Element element, String constraintName) {
        List<XSDIdentityConstraintDefinition> constraints = findIdentityConstraints(element);
        for (XSDIdentityConstraintDefinition c : constraints) {
            if (c.getName().equals(constraintName)) {
                return c;
            }
        }

        return null;
    }
}
