// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.job;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubMonitor;

import silecs.control.core.DeployProjectNature;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;

public class GenerateDeployJob extends BaseProjectJob {

    private static final String JOB_NAME = "Generate SILECS Deploy Job";

    IFile fesaDeployDoc_;
    boolean createdNewDeployDoc_;

    public GenerateDeployJob(IProject project, IFile fesaDeployDoc, boolean createdNewDeployDoc) {
        super(JOB_NAME, project);
        fesaDeployDoc_ = fesaDeployDoc;
        createdNewDeployDoc_ = createdNewDeployDoc;
    }

    @Override
    public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {

        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        String workspacePath = workspaceRoot.getLocation().toFile().getPath();

        monitor.beginTask("Generating Code", 1);
        generateDeploy(workspacePath, SubMonitor.convert(monitor, 1));
        monitor.done();

        return computeReturnStatus();
    }

    private void generateDeploy(String workspacePath, IProgressMonitor monitor) {
        String projectName = project.getName();

        try {
            Version silecsVersion = SilecsUtils.getSilecsVersionFromProject(project);

            monitor.beginTask("Generating: " + projectName, 2);

            ConsoleHandler.printMessage("Generating Code for: " + projectName);

            String silecsCliPath = MainPreferencePage.getSilecsCliPath(silecsVersion);
            String[] command;
            String fileAbsPath = SilecsUtils.getSilecsDocument(project).getRawLocation().toOSString();
            command = new String[] { silecsCliPath, "-g", fileAbsPath };

            Process process = Runtime.getRuntime().exec(command);
            process.waitFor();

            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                ConsoleHandler.printMessage(line);
            }

            input.close();

            BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            while ((line = error.readLine()) != null) {
                ConsoleHandler.printError(line);
            }

            error.close();

            DeployProjectNature.addFESADeployUnitNature(project, monitor);
            DeployProjectNature.addBuildSpecs(project);

            project.refreshLocal(IResource.DEPTH_INFINITE, null);

            ConsoleHandler.printMessage("Succeeded in generated code for " + projectName);

            if (createdNewDeployDoc_)
                SilecsUtils.openInEditor(fesaDeployDoc_);

            monitor.worked(1);

        } catch (Exception e) {
            e.printStackTrace();
            ConsoleHandler.printError("Failed to generate code for " + projectName);
            ConsoleHandler.printStackTrace(e);
        }
    }
}