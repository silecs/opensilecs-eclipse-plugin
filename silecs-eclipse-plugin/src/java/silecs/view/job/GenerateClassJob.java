// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.job;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubMonitor;

import silecs.control.core.DesignProjectNature;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;

public class GenerateClassJob extends BaseProjectJob {

    private static final String JOB_NAME = "Generate SILECS Class Job";

    IFile fesaDesignXML;
    IFile silecsDesignXML;

    public GenerateClassJob(IProject project, IFile silecsDesignDoc, IFile fesaDesignDoc) {
        super(JOB_NAME, project);
        fesaDesignXML = fesaDesignDoc;
        silecsDesignXML = silecsDesignDoc;
    }

    @Override
    public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {

        IWorkspaceRoot workspaceRoot = ResourcesPlugin.getWorkspace().getRoot();
        String workspacePath = workspaceRoot.getLocation().toFile().getPath();

        monitor.beginTask("Generate Code", 1);

        generateCode(workspacePath, SubMonitor.convert(monitor, 1));
        DesignProjectNature.addFESAClassNature(project, monitor);
        DesignProjectNature.addBuildSpecs(project);
        monitor.done();

        return computeReturnStatus();
    }

    boolean generateCode(String workspacePath, IProgressMonitor monitor) {
        boolean sucess = true;

        String projectName = project.getName();
        try {

            Version silecsVersion = SilecsUtils.getSilecsVersionFromProject(project);
            ConsoleHandler.printMessage("Generating code for project '" + projectName + "'");

            String silecsCliScript = MainPreferencePage.getSilecsCliPath(silecsVersion);
            String[] command;
            String fileAbsPath = silecsDesignXML.getRawLocation().toOSString();
            command = new String[] { silecsCliScript, "-g", fileAbsPath };

            Process process = Runtime.getRuntime().exec(command);
            process.waitFor();

            BufferedReader input = new BufferedReader(new InputStreamReader(process.getInputStream()));
            String line;
            while ((line = input.readLine()) != null) {
                ConsoleHandler.printMessage(line);
            }
            input.close();

            BufferedReader error = new BufferedReader(new InputStreamReader(process.getErrorStream()));
            while ((line = error.readLine()) != null) {
                ConsoleHandler.printError(line);
                sucess = false;
            }
            error.close();

            project.refreshLocal(IResource.DEPTH_INFINITE, null);
            // SilecsUtils.openInEditor(fesaDesignXML);

        } catch (Exception e) {
            e.printStackTrace();
            ConsoleHandler
                    .printError("Failed to generate code for project '" + projectName + "' Reason: " + e.getMessage());
            ConsoleHandler.printStackTrace(e);
            return false;
        }

        if (sucess)
            ConsoleHandler.printMessage("Successfully generated code for project '" + projectName + "'");
        else
            ConsoleHandler.printError("Failed to generate code for project ' " + projectName + "'");

        return sucess;

    }
}
