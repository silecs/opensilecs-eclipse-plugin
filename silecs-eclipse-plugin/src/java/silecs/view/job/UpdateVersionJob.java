// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.job;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.SubMonitor;

import silecs.control.core.DeployProjectNature;
import silecs.control.core.DesignProjectNature;
import silecs.model.exception.SilecsException;
import silecs.utils.OSExecute;
import silecs.utils.SilecsUtils;
import silecs.utils.Version;
import silecs.utils.SilecsConstants;
import silecs.view.console.ConsoleHandler;
import silecs.view.preferences.MainPreferencePage;

public class UpdateVersionJob extends BaseProjectJob {
    private static final String JOB_NAME = "Update SILECS Version Job";

    Version newVersion_;
    Version oldVersion_;

    class MigrationScript {
        public File file_;
        public Version oldVersion_;
        public Version newVersion_;

        public MigrationScript(Version oldVersion, Version newVersion) {
            String codeGenBasePath = MainPreferencePage.getCodeGenBasePath(newVersion);
            String oldVersionUnderscored = oldVersion.toString_underscored_TinyAsX();
            String newVersionUnderscored = newVersion.toString_underscored_TinyAsX();
            String migrationScript = codeGenBasePath + "/migration/" + oldVersionUnderscored + "to"
                    + newVersionUnderscored + ".py";

            file_ = new File(migrationScript);
            oldVersion_ = oldVersion;
            newVersion_ = newVersion;
        }
    }

    public UpdateVersionJob(IProject project, Version newVersion) {
        super(JOB_NAME, project);
        newVersion_ = newVersion;
    }

    @Override
    public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {
        monitor.beginTask("Updating Silecs Version", 1);
        updateVersion(monitor);
        monitor.done();
        return computeReturnStatus();
    }

    boolean updateVersion(IProgressMonitor monitor) {
        try {
            Vector<MigrationScript> migrationScripts = gatherNeededScripts(monitor);
            monitor.beginTask("Apply migration scripts on project " + project.getName(), migrationScripts.size());
            for (MigrationScript script : migrationScripts)
                executeMigrationScript(script, SubMonitor.convert(monitor, 1));
            project.refreshLocal(IResource.DEPTH_INFINITE, null);
        } catch (Exception e) {
            e.printStackTrace();
            ConsoleHandler.printError("Failed to update SILECS version of: " + project.getName());
            ConsoleHandler.printStackTrace(e);
            return false;
        }

        monitor.done();
        ConsoleHandler.printMessage("Successfully updated SILECS version of: " + project.getName());
        return true;
    }

    void executeMigrationScript(MigrationScript migrationScript, IProgressMonitor monitor)
            throws CoreException, Exception {
        String newXMLSchema = MainPreferencePage.getModelBasePath(migrationScript.newVersion_) + "/";

        if (SilecsUtils.hasSilecsDesign(project)) {
            newXMLSchema += SilecsConstants.DESIGN_SCHEMA_XSD;
        } else if (SilecsUtils.hasSilecsDeploy(project)) {
            newXMLSchema += SilecsConstants.DEPLOY_SCHEMA_XSD;
        } else
            throw new Exception("No silecs document found in project" + project.getName());

        String silecsDocumentPath = SilecsUtils.getSilecsDocument(project).getRawLocation().makeAbsolute().toOSString();
        OSExecute.executeCommand("python3", migrationScript.file_.getCanonicalPath(), silecsDocumentPath, newXMLSchema,
                migrationScript.oldVersion_.toString(), migrationScript.newVersion_.toString());
        ConsoleHandler.printMessage(
                "Updating Silecs Version from '" + oldVersion_.toString() + "'to '" + newVersion_.toString() + "'");

        monitor.worked(1);
    }

    Vector<MigrationScript> gatherNeededScripts(IProgressMonitor monitor) throws Exception {
        oldVersion_ = SilecsUtils.getSilecsVersionFromProject(project);

        if (newVersion_.isOlderOrEqualThan(oldVersion_)) {
            throw new SilecsException("Migration from " + oldVersion_ + " to " + newVersion_
                    + " not possible. The target-version always needto newer than the current version");
        }

        String codeGenBasePath = MainPreferencePage.getCodeGenBasePath(newVersion_);
        List<String> availableSilecsVersions_string = SilecsUtils.readOutAvailableSilecsVersions();
        Collections.reverse(availableSilecsVersions_string); // start with smallest version-number
        List<Version> availableSilecsVersions = new ArrayList<Version>();
        for (String versionString : availableSilecsVersions_string)
            availableSilecsVersions.add(new Version(versionString));

        Vector<MigrationScript> migrationScriptsToApply = new Vector<MigrationScript>();

        Version lastScriptVersion = null;
        for (Iterator<Version> iter = availableSilecsVersions.iterator(); iter.hasNext();) {
            Version currentIterVersion = iter.next();
            if (currentIterVersion.isOlderOrEqualThan(oldVersion_))
                continue;// we only search for >= old version

            if (currentIterVersion.isNewerThan(oldVersion_)) {
                if (currentIterVersion.isNewerThan(newVersion_))
                    break;

                MigrationScript migrationScript = null;
                if (lastScriptVersion == null)
                    migrationScript = new MigrationScript(oldVersion_, currentIterVersion);
                else
                    migrationScript = new MigrationScript(lastScriptVersion, currentIterVersion);
                // ConsoleHandler.printMessage(migrationScript.file_.getCanonicalPath());
                if (!migrationScript.file_.exists())
                    continue; // no migration-script for this version available, let's continue to search
                lastScriptVersion = currentIterVersion;
                migrationScriptsToApply.add(migrationScript);
            }
        }

        if (migrationScriptsToApply.isEmpty()) {

            ConsoleHandler.printWarning("There is no Migration Script for an update from: '" + oldVersion_.toString()
                    + "' to '" + newVersion_.toString()
                    + "'. Using generic migration-script. Only basic xml-version strings will be changed.");
            MigrationScript migrationScript = new MigrationScript(oldVersion_, newVersion_);
            migrationScript.file_ = new File(codeGenBasePath + "/migration/migrationBase.py");
            if (!migrationScript.file_.exists()) {
                throw new SilecsException("The Migration Script: '" + migrationScript.file_.getAbsolutePath()
                        + "' does not exist. Please check your Eclipse-Silecs preferences!");
            }
            migrationScriptsToApply.add(migrationScript);
        }

        return migrationScriptsToApply;
    }
}
