// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.job;

import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.PlatformUI;
import org.eclipse.wst.sse.core.StructuredModelManager;
import org.eclipse.wst.sse.core.internal.provisional.IStructuredModel;
import org.eclipse.wst.xml.core.internal.document.AttrImpl;
import org.eclipse.wst.xml.core.internal.document.ElementImpl;
import org.eclipse.wst.xml.core.internal.document.NodeImpl;
import org.eclipse.wst.xml.core.internal.provisional.document.IDOMModel;
import org.eclipse.xsd.XSDIdentityConstraintDefinition;
import org.eclipse.xsd.XSDXPathDefinition;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import silecs.control.validation.Validator;
import silecs.model.document.SilecsDocumentError;
import silecs.model.document.XmlBasedDocument;
import silecs.model.exception.SilecsException;
import silecs.view.console.ConsoleHandler;
import silecs.view.editor.XMLMultiPageEditor;
import silecs.view.editor.internal.SchemaHelper;
import silecs.view.marker.SilecsMarker;

@SuppressWarnings("restriction")
public class ValidationJob extends BaseFileJob {
    private static final String JOB_NAME = "silecs validation job";

    public ValidationJob(IFile file) {
        this(JOB_NAME, file);

    }

    public ValidationJob(String name, IFile file) {
        super(name, file);
    }

    @Override
    public IStatus runInWorkspace(IProgressMonitor monitor) throws CoreException {
        boolean hasErros = false;
        try {
            // monitor.beginTask(Messages.get(Message.VALIDATION_PROCESS) + " ...",
            // silecsFiles.size() * 2);
            monitor.beginTask("Validation Job", 1);
            try {
                hasErros = !isValid(file, SubMonitor.convert(monitor, 1));
                file.refreshLocal(IResource.DEPTH_ZERO, null);
            } catch (Exception e) {
                failures.put(file, e);
            }

            return computeReturnStatus();
        } finally {
            // Open problem view if there are some errors
            if (hasErros) {
                Display.getDefault().syncExec(new Runnable() {

                    @Override
                    public void run() {
                        IWorkbenchPage activepage = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
                                .getActivePage();
                        IViewPart pbv = activepage.findView("org.eclipse.ui.views.ProblemView");
                        if (pbv != null) {
                            activepage.bringToTop(pbv);
                        }
                    }
                });
            }

            monitor.done();
        }
    }

    protected boolean isValid(IFile silecsFile, IProgressMonitor monitor) {
        IProgressMonitor mon = monitor;
        if (mon == null) {
            mon = new NullProgressMonitor();
        }

        try {
            mon.beginTask("Validation", 1);

            List<SilecsDocumentError> errors = null;

            errors = Validator.validate(silecsFile);

            final XMLMultiPageEditor editor = XMLMultiPageEditor.getOpenedEditor(silecsFile);

            updateMarkers(silecsFile, errors, editor);

            // return true if document is valid
            if (errors.isEmpty()) {
                ConsoleHandler.printMessage("Document " + silecsFile.getName() + " is valid");
                return true;
            } else {
                for (SilecsDocumentError error : errors)
                    ConsoleHandler.printMessage("Error found: " + error.toString());
            }

            ConsoleHandler.printError("Document " + silecsFile.getName() + " is not valid");
            return false;
        } finally {
            mon.done();
        }
    }

    private void updateMarkers(IFile silecsFile, List<SilecsDocumentError> errors, XMLMultiPageEditor editor) {
        // remove old markers
        try {
            silecsFile.deleteMarkers(SilecsMarker.TYPE, false, IResource.DEPTH_ZERO);
        } catch (CoreException e) {
            ConsoleHandler.printStackTrace(e);
        }

        for (SilecsDocumentError error : errors) {
            try {
                createMarker(silecsFile, error, editor);
            } catch (Exception e) {
                ConsoleHandler.printMessage("Could not create marker for " + error);
            }
        }

    }

    private IMarker createMarker(IFile file, SilecsDocumentError error, XMLMultiPageEditor editor)
            throws CoreException, SilecsException {
        IStructuredModel model = StructuredModelManager.getModelManager().getExistingModelForRead(file);
        if (model instanceof IDOMModel) {
            try {
                NodeImpl hintNode = null;
                XmlBasedDocument xmlDocument = new XmlBasedDocument(((IDOMModel) model).getDocument());

                if (error.xPath != null) {
                    hintNode = (NodeImpl) xmlDocument.getSingleNode(error.xPath);
                }

                // first try to get accurate not thanks to the schema
                if (error.isIdentityConstraintError() && hintNode instanceof Element) {
                    XSDIdentityConstraintDefinition def = SchemaHelper.findIdentityConstraint((Element) hintNode,
                            error.violatedKey);

                    // def = null can happen when eclipse has not finished to build the schema
                    if (def != null) {
                        List<Node> candidates = xmlDocument.getNodes(hintNode, def.getSelector().getValue());
                        for (Node candidate : candidates) {
                            for (XSDXPathDefinition field : def.getFields()) {
                                List<Node> values = xmlDocument.getNodes(candidate, field.getValue());
                                for (Node v : values) {
                                    if ((v instanceof AttrImpl
                                            && ((AttrImpl) v).getValue().equals(error.incorrectValue))
                                            || (v instanceof Element) && ((ElementImpl) v).getTextContent()
                                                    .equals(error.incorrectValue)) {
                                        return SilecsMarker.create(file, error, (NodeImpl) v);
                                    }
                                }
                            }
                        }
                    }
                }

                // try to get the node thanks to the document position
                if (editor != null) {
                    NodeImpl node = editor.getNodeAtPosition(error.lineNumber, error.columnNumber);
                    if (node != null) {
                        hintNode = node;
                    }
                }

                // if fail, use the hint node found with the xpath provided by the validator
                if (hintNode != null) {
                    return SilecsMarker.create(file, error, hintNode);
                }
            } catch (SilecsException e) {
                ConsoleHandler.printMessage("Could not get precise location for marker: " + e.getMessage());
                throw e;
            }
        }
        return SilecsMarker.create(file, error);
    }

}
