// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.explorer;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.widgets.Display;

public class ProjectContent implements ITreeContentProvider, IResourceChangeListener {

    private Viewer attachedViewer;

    private static final Object[] NO_OBJECTS = new Object[0];

    public ProjectContent() {
        ResourcesPlugin.getWorkspace().addResourceChangeListener(this);
    }

    @Override
    public void dispose() {
        ResourcesPlugin.getWorkspace().removeResourceChangeListener(this);
    }

    @Override
    public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
        attachedViewer = viewer;
    }

    @Override
    public Object[] getElements(Object inputElement) {
        if (inputElement instanceof IWorkspaceRoot) {
            // if root return the projects
            return ((IWorkspaceRoot) inputElement).getProjects();
        } else if (inputElement instanceof IProject || inputElement instanceof IFolder) {
            // if folder or project, return the content
            return getChildren(inputElement);
        } else {
            return NO_OBJECTS;
        }
    }

    @Override
    public Object[] getChildren(Object parentElement) {
        if (parentElement instanceof IProject) {
            IProject p = (IProject) parentElement;
            try {
                return p.members();
            } catch (CoreException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        } else if (parentElement instanceof IFolder) {
            try {
                return ((IFolder) parentElement).members();
            } catch (CoreException e) {
                // The resource is not found
                return NO_OBJECTS;
            }
        }
        return NO_OBJECTS;
    }

    @Override
    public Object getParent(Object element) {
        if (element instanceof IProject) {
            return ((IProject) element).getWorkspace().getRoot();
        } else if (element instanceof IFolder) {
            IFolder folder = (IFolder) element;
            if (folder.getParent() instanceof IProject) {
                // parent of folder is the project
                return folder.getProject();
            } else {
                return folder.getParent();
            }
        } else if (element instanceof IFile) {
            IFile file = (IFile) element;
            return file.getParent();
        } else {
            return null;
        }
    }

    @Override
    public boolean hasChildren(Object element) {
        IResource[] members = null;

        try {
            if (element instanceof IFolder) {
                members = ((IFolder) element).members();
            }
        } catch (CoreException e) {
            // error while accessing members
        }

        // has children if element is project or not empty folder
        return element instanceof IProject || (members != null && members.length > 0);
    }

    @Override
    public void resourceChanged(IResourceChangeEvent event) {
        if (attachedViewer != null && !attachedViewer.getControl().isDisposed()) {

            // make the UI thread update the viewer
            Display.getDefault().asyncExec(new Runnable() {

                @Override
                public void run() {
                    attachedViewer.refresh();
                }
            });
        }
    }

}
