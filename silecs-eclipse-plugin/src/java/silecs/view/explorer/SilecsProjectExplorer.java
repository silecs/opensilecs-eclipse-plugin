// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.view.explorer;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IStatusLineManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.ILabelProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.navigator.CommonNavigator;
import org.eclipse.ui.navigator.CommonViewer;
import org.eclipse.ui.navigator.ICommonActionConstants;

import silecs.control.core.DeployProjectNature;
import silecs.control.core.DesignProjectNature;
import silecs.utils.SilecsUtils;
import silecs.view.console.ConsoleHandler;

/**
 * This class is a subclass of CommonNavigator that provide changes concerning
 * expanding tree.
 * 
 * @see CommonNavigator
 */
public class SilecsProjectExplorer extends CommonNavigator {

    public static final String VIEW_ID = "silecs.view.explorer";

    @Override
    public void createPartControl(Composite aParent) {
        super.createPartControl(aParent);
        CommonViewer viewer = getCommonViewer();

        IActionBars bars = getViewSite().getActionBars();
        final IStatusLineManager lineManager = bars.getStatusLineManager();

        final ILabelProvider labelProvider = (ILabelProvider) viewer.getLabelProvider();

        viewer.addPostSelectionChangedListener(new ISelectionChangedListener() {

            @Override
            public void selectionChanged(SelectionChangedEvent event) {

                ISelection aSelection = event.getSelection();
                if (aSelection != null && !aSelection.isEmpty() && aSelection instanceof IStructuredSelection) {
                    IStructuredSelection selection = (IStructuredSelection) aSelection;
                    IResource file = null;

                    if (selection.size() == 1 && selection.getFirstElement() instanceof IProject) {
                        IProject project = (IProject) selection.getFirstElement();

                        // If project is silecs project but does not have silecs nature, we try to add
                        // it
                        try {
                            SilecsUtils.prepareSilecsProjectNatures(project);
                        } catch (Exception e) {
                            ConsoleHandler.printMessage("Exception when adding natures: " + e.getMessage());
                            ConsoleHandler.printStackTrace(e);
                        }

                        try {
                            file = SilecsUtils.getSilecsDocument(project);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else if (selection.size() == 1 && selection.getFirstElement() instanceof IFile) {
                        file = (IFile) selection.getFirstElement();
                    }

                    if (file != null) {
                        Image img = null;
                        img = labelProvider.getImage(((IStructuredSelection) aSelection).getFirstElement());
                        long modificationTime = file.getLocalTimeStamp() / 1000;

                        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-YYYY   HH:mm:ss");
                        LocalDateTime dateAndTime = LocalDateTime.ofEpochSecond(modificationTime, 0,
                                ZoneOffset.ofHours(2));

                        lineManager.setMessage(img,
                                "Last modfication: " + formatter.format(dateAndTime) + " : " + file.getFullPath());
                    }
                }

            }
        });

    }

    /**
     * This method is responsible for expanding the project tree from the root to
     * the silecs file(*.silecsdesign or *.silecsdeploy)
     */
    @Override
    protected void handleDoubleClick(DoubleClickEvent anEvent) {

        IStructuredSelection selection = (IStructuredSelection) anEvent.getSelection();
        Object element = selection.getFirstElement();
        if (element instanceof IProject && !((IProject) element).isOpen()) {
            try {
                ((IProject) element).open(new NullProgressMonitor());
            } catch (CoreException e) {
                e.printStackTrace();
            }
            return;

        } else if (element instanceof IProject && ((IProject) element).isOpen()) {
            List<IProject> projectList = new ArrayList<IProject>();
            projectList.add((IProject) element);

            IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();

            expandHelper(root, projectList);
            return;
        }
        super.handleDoubleClick(anEvent);
    }

    /**
     * This method expands elements in the tree. Since the tool does not have
     * knowledge about project structure it must be done recursively.
     * 
     * @param root     {@link WorkspaceRoot}
     * @param projects List of projects that need to be expanded
     */
    public void expandHelper(IWorkspaceRoot root, List<IProject> projects) {
        for (IProject project : projects) {
            if (project.isOpen()) {
                try {
                    IFile file = SilecsUtils.getSilecsDocument(project);
                    if (file == null)
                        continue;

                    IContainer parent = file.getParent();
                    List<Object> toExpand = new ArrayList<>();

                    do {
                        toExpand.add(0, parent);
                    } while ((parent = parent.getParent()) != root);
                    boolean expand = !getCommonViewer().getExpandedState(project);
                    for (Object o : toExpand)
                        expand(o, expand);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Expands single element in the viewer
     * 
     * @param element
     * @return true if expanded
     */
    private boolean expand(Object element, boolean expand) {
        IAction openHandler = getViewSite().getActionBars().getGlobalActionHandler(ICommonActionConstants.OPEN);

        if (openHandler == null) {

            TreeViewer viewer = getCommonViewer();
            if (viewer.isExpandable(element)) {
                viewer.setExpandedState(element, expand);
                return true;
            }
        }
        return false;
    }

}
