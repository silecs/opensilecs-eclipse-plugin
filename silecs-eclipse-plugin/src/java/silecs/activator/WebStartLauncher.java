// Copyright 2016 CERN and GSI
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package silecs.activator;

import java.util.Properties;
import java.util.Set;

import org.eclipse.equinox.launcher.WebStartMain;

/**
 * This class is only used by jnlp launcher. It is not used locally.
 */
@SuppressWarnings("restriction")
public class WebStartLauncher {

    public static void main(String[] args) {
        Properties properties = System.getProperties();
        // copy properties to avoid ConcurrentModificationException
        Properties copiedProperties = new Properties();
        copiedProperties.putAll(properties);
        Set<Object> keys = copiedProperties.keySet();
        for (Object key : keys) {
            if (key instanceof String) {
                String keyString = (String) key;
                if (keyString.startsWith("jnlp.")) {
                    // re set all properties starting with the jnlp-prefix
                    // and set them without the prefix
                    String property = System.getProperty(keyString);
                    String replacedKeyString = keyString.replaceFirst("jnlp.", "");

                    System.setProperty(replacedKeyString, property);
                }
            }
        }

        WebStartMain.main(args);
    }
}